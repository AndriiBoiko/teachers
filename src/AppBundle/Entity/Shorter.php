<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shorter
 *
 * @ORM\Table(name="shorter")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShorterRepository")
 */
class Shorter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="original_link", type="text")
     */
    private $originalLink;

    /**
     * @var string
     *
     * @ORM\Column(name="short_link", type="text")
     */
    private $shortLink;

    /**
     * @var int
     *
     * @ORM\Column(name="counter", type="integer", nullable=true)
     */
    private $counter;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set originalLink
     *
     * @param string $originalLink
     * @return Shorter
     */
    public function setOriginalLink($originalLink)
    {
        $this->originalLink = $originalLink;

        return $this;
    }

    /**
     * Get originalLink
     *
     * @return string
     */
    public function getOriginalLink()
    {
        return $this->originalLink;
    }

    /**
     * Set shortLink
     *
     * @param string $shortLink
     * @return Shorter
     */
    public function setShortLink($shortLink)
    {
        $this->shortLink = $shortLink;

        return $this;
    }

    /**
     * Get shortLink
     *
     * @return string
     */
    public function getShortLink()
    {
        return $this->shortLink;
    }

    /**
     * Set counter
     *
     * @param integer $counter
     * @return Shorter
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * Get counter
     *
     * @return integer
     */
    public function getCounter()
    {
        return $this->counter;
    }
}
