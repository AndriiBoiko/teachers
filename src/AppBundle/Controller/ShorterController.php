<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Shorter;
use AppBundle\Form\ShorterType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Shorter controller.
 *
 * @Route("/")
 */
class ShorterController extends Controller
{
    /**
     * Lists all Shorter entities.
     *
     * @Route("/", name="shorter_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $shorters = $em->getRepository('AppBundle:Shorter')->findAll();

        return $this->render('shorter/index.html.twig', [
            'shorters' => $shorters,
        ]);

//        return $this->redirectToRoute('shorter_new');
    }

    /**
     * Creates a new Shorter entity.
     *
     * @Route("/new", name="shorter_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $shorter = new Shorter();
        $form = $this->createForm('AppBundle\Form\ShorterType', $shorter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $shorter->setShortLink('1');
            $em->persist($shorter);
            $em->flush();

            return $this->redirectToRoute('shorter_show', ['id' => $shorter->getId()]);
        }

        return $this->render('shorter/new.html.twig', [
            'shorter' => $shorter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Shorter entity.
     *
     * @Route("/{id}", name="shorter_show")
     * @Method("GET")
     */
    public function showAction(Shorter $shorter)
    {
        $shorter->setCounter($shorter->getCounter() + 1);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirect($shorter->getOriginalLink());
    }

    /**
     * Displays a form to edit an existing Shorter entity.
     *
     * @Route("/{id}/edit", name="shorter_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Shorter $shorter)
    {
        $deleteForm = $this->createDeleteForm($shorter);
        $editForm = $this->createForm('AppBundle\Form\ShorterType', $shorter);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($shorter);
            $em->flush();

            return $this->redirectToRoute('shorter_edit', ['id' => $shorter->getId()]);
        }

        return $this->render('shorter/edit.html.twig', [
            'shorter' => $shorter,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a Shorter entity.
     *
     * @Route("/{id}", name="shorter_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Shorter $shorter)
    {
        $form = $this->createDeleteForm($shorter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($shorter);
            $em->flush();
        }

        return $this->redirectToRoute('shorter_index');
    }

    /**
     * Creates a form to delete a Shorter entity.
     *
     * @param Shorter $shorter The Shorter entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Shorter $shorter)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('shorter_delete', ['id' => $shorter->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }

    private function createShortLink(){
        do {
            $timeMD5 = md5(time());
            $short = substr($timeMD5,0,6);
            $check = $this->getDoctrine()->getRepository('AppBundle:Shorter')->findBy(['shortLink' => $short]);
        } while (!$check);

        return $short;
    }
}
